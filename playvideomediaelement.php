<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>MediaElement.js 3.0 - Postroll Plugin</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
    <link rel="icon" href="favicon.ico" type="image/x-icon">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/mediaelement/4.2.6/mediaelementplayer.css">
    <link rel="stylesheet" href="css/postroll.css">
    <link rel="stylesheet" href="css/demo.css">
</head>
<body>
<script src="https://cdnjs.cloudflare.com/ajax/libs/mediaelement/4.2.6/mediaelement-and-player.min.js"></script>
<script src="js/postroll.js"></script>
<script src="js/ads.js"></script>
<script src="js/ads-vast-vpaid.min.js"></script>
<?php
if (isset($_GET["video"])) {
    $data = '<div class="media-wrapper">
                <video id="player1" width="750" height="421" controls preload="none">
                    <source src="https://commondatastorage.googleapis.com/gtv-videos-bucket/CastVideos/mp4/BigBuckBunny.mp4"
                            type="video/mp4">
                </video>
            </div>';
    $data .=  "<script>
        new MediaElementPlayer('player1', {
            vastAdTagUrl: 'http://localhost:8080/affnet/higgsup/v1/api/vast/1',
                vastAdsType : 'VAST',
                adsPrerollAdEnableSkip : true,
                adsPrerollAdSkipSeconds: 10,
                features: ['playpause', 'current', 'progress', 'duration', 'volume', 'fullscreen', 'postroll', 'ads', 'vast']
            });
        </script>";
    echo $data;
}
?>
</body>
</html>