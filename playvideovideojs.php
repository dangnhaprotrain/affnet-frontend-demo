<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>Video.js VAST Example</title>

    <link href="http://vjs.zencdn.net/4.7.1/video-js.css" rel="stylesheet">
    <link href="lib/videojs-contrib-ads/videojs.ads.css" rel="stylesheet" type="text/css">
    <link href="css/videojs.vast.css" rel="stylesheet" type="text/css">

    <style type="text/css">
        .description {
            background-color:#eee;
            border: 1px solid #777;
            padding: 10px;
            font-size: .8em;
            line-height: 1.5em;
            font-family: Verdana, sans-serif;
        }
        .example-video-container {
            display: inline-block;
        }
    </style>
    <!--[if lt IE 9]><script src="lib/es5.js"></script><![endif]-->
    <script src="http://vjs.zencdn.net/4.7.1/video.js"></script>
    <script src="lib/videojs-contrib-ads/videojs.ads.js"></script>

    <script src="lib/vast-client.js"></script>
    <script src="lib/videojs.vast.js"></script>
</head>
<body>
<p class="description">Video.js VAST plugin examples.</p>

<div class="example-video-container">

    <video id="vid2" class="video-js vjs-default-skin" autoplay controls preload="auto"
           poster="http://video-js.zencoder.com/oceans-clip.png"
           data-setup='{}'
           width='640'
           height='400'
    >
        <?php
            if (isset($_GET["video"])) {
                echo '
                <script>
                var vid2 = videojs("vid2");
           
                vid2.muted(true);
                vid2.ads();
                vid2.vast({
                    url: "http://localhost:8080/affnet/higgsup/v1/api/vast/1"
                });
                </script>
                <source src="'.$_GET["video"].'" type="video/mp4">';
            }
        ?>
        <p>Video Playback Not Supported</p>
    </video>
</div>

</body>
</html>
<?php