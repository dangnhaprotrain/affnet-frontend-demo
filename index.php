<!DOCTYPE html>
<html>
   <head>
      <title>Page Title</title>
      <meta charset="utf-8" />
      <meta name="viewport" content="width=device-width, initial-scale=1" />
      <link rel="stylesheet" href="assets/css/main.css" />
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
      <link href="http://vjs.zencdn.net/4.12/video-js.css" rel="stylesheet">
      <style>
         input[type="radio"] {
         -moz-appearance: none;
         -webkit-appearance: radio;
         -ms-appearance: none;
         appearance: none;
         display: block;
         float: left;
         margin-right: 0;
         opacity: 1;
         width: 1em;
         z-index: -1;
         }
		 #header.hide, #header.preview{
			 min-height: 40px;
		 }
      </style>
   </head>
   <body>
   <form id="myForm">
   <!-- Header -->
			
      <!-- Header -->
      <header id="header">
         <label class="custom-control custom-radio">
         <input class="alt" id="radioStacked3" name="radio-stacked" type="radio" class="custom-control-input" value="jw" >
         <span class="custom-control-indicator"></span>
         <span class="custom-control-description">JW-Player</span>
         </label>
         <label class="custom-control custom-radio">
         <input class="alt" id="radioStacked4" name="radio-stacked" type="radio" class="custom-control-input" value="media">
         <span class="custom-control-indicator"></span>
         <span class="custom-control-description">Media Element</span>
         </label>
         <label class="custom-control custom-radio">
         <input class="alt" id="radioStacked4" name="radio-stacked" type="radio" class="custom-control-input" value="videojs">
         <span class="custom-control-indicator"></span>
         <span class="custom-control-description">Video JS</span>
         </label>
		 <div style="clear:both"></div>
		 <div>
			<a href="#" class="button hidden"><span>Confirm</span></a>
		 </div>
		 
      </header>
      <div id="main">
         <div class="inner">
            <div class="columns">
               <?php
                  $arrayVideo = ["http://sachinchoolur.github.io/lightGallery/static/img/thumb-v-y-2.jpg" => "//vo.fod4.com/v/c5891cc51c/v1280-q2.mp4",
                  	"http://sachinchoolur.github.io/lightGallery/static/img/thumb-v-y-1.jpg" => "//vo.fod4.com/v/d1adb9698b/v1280-q2.mp4",
                  	"http://sachinchoolur.github.io/lightGallery/static/img/thumb-v-v-2.jpg" => "//vo.fod4.com/v/b5a47411c7/v1280-q2.mp4"];
                  $i = 0;
                  foreach ($arrayVideo as $thumbnail => $video) {
                  	echo '<div class="image fit videodata col-md-3 col-sm-4 col-xs-6" data-poster="' . $video . '" >
                  			<img class="img-responsive" src="' . $thumbnail . '" />
                  			</div>';
                  	$i++;
                  }
                  
                  ?>
               <script>
                  $(".videodata").click(function () {
                  	var video = this.getAttribute("data-poster");
                  	var player = $('input[name=radio-stacked]:checked', '#myForm').val();
                  	if (player == "jw") {
                  		window.location = "//playvideo.php?video=" + video;
                  	} else if (player == "media") {
                  		window.location = "/playvideomediaelement.php?video=" + video;
                  	} else {
                  		window.location = "/playvideovideojs.php?video=" + video;
                  	}
                  });
               </script>
            </div>
         </div>
      </div>
      <!-- Footer -->
      <footer id="footer">
         <a href="#" class="info fa fa-info-circle"><span>About</span></a>
         <div class="inner">
            <div class="content">
               <h3>AFFNET DEMO</h3>
               <p>Intergration between ads video and pub</p>
            </div>
            <div class="copyright">
               <h3>Follow me</h3>
               <ul class="icons">
                  <li><a href="#" class="icon fa-twitter"><span class="label">Twitter</span></a></li>
                  <li><a href="#" class="icon fa-facebook"><span class="label">Facebook</span></a></li>
                  <li><a href="#" class="icon fa-instagram"><span class="label">Instagram</span></a></li>
                  <li><a href="#" class="icon fa-dribbble"><span class="label">Dribbble</span></a></li>
               </ul>
               &copy;HIGGSUP
            </div>
         </div>
      </footer>
      <div style="margin-left:100px">
      </div>
      <script src="assets/js/jquery.min.js"></script>
      <script src="assets/js/skel.min.js"></script>
      <script src="assets/js/util.js"></script>
      <script src="assets/js/main.js"></script>
	  </form>
   </body>
</html>